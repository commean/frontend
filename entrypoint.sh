#!/bin/sh
for file in /usr/share/nginx/html/assets/*.js;
do
  echo $file
  if [ ! -f $file.js ]; then
    cp $file $file.tmp
  fi
  envsubst '$VITE_BACKEND_HOST' < $file.tmp > $file
  rm $file.tmp
done
echo $VITE_BACKEND_HOST
echo "Starting Nginx"
nginx -g 'daemon off;'