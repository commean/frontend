# Frontend
![beta](https://img.shields.io/docker/v/commean/frontend/beta?label=beta&style=for-the-badge) ![alpha](https://img.shields.io/docker/v/commean/frontend/alpha?label=alpha&style=for-the-badge)

This Repository contains the frontend of Commean.
- [ ] Show a map of the centered city
- [ ] Show a small icon for each traffic light
- [ ] Show more detailed info in a dashboard

