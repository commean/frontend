import axios from "axios";

const baseURL = `${VITE_BACKEND_HOST}/api/v1`;

export default axios.create({
  baseURL,
});