ARG BUILD_PATH=/commean/frontend
ARG SERVE_PATH=/usr/share/nginx/html


FROM node:lts-alpine3.15 AS build_env
ARG BUILD_PATH

WORKDIR ${BUILD_PATH}
ADD . ${BUILD_PATH}

RUN npm install
RUN npm run build


FROM nginx:stable-alpine
ARG BUILD_PATH
ARG SERVE_PATH
EXPOSE 80/tcp

COPY --from=build_env ${BUILD_PATH}/dist ${SERVE_PATH}
COPY docker/nginx.conf /etc/nginx/conf.d/default.conf
COPY entrypoint.sh /
RUN chmod +x /entrypoint.sh
ENTRYPOINT [ "/entrypoint.sh" ]
